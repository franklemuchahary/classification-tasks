
# coding: utf-8

# In[3]:

import pandas as pd
import numpy as np
import math
import string
from collections import Counter


# In[4]:

dtype_dict = {'name':str, 'review':str, 'rating':float}
products = pd.read_csv('amazon_baby.csv', dtype=dtype_dict)


# In[85]:

products.head()


# ### cleaning data and creating labels

# In[6]:

products = products.fillna({'review':''})


# In[7]:

#function to remove all punctuations
def remove_punctuations(text):
    return text.translate(None, string.punctuation)
    
products['review_clean'] = products['review'].apply(remove_punctuations)


# In[8]:

#function to get word counts
def word_count_vector(text):
    words = text.split()
    return Counter(words)

products['word_count'] = products['review_clean'].apply(word_count_vector)


# In[9]:

products = products.drop('word_count', axis=1)


# In[10]:

products = products[products['rating']!=3]


# In[11]:

#adding sentiments +1 for all greater than 3 and -1 for all less than 3
products['sentiment'] = products['rating'].apply(lambda rating: +1 if rating >3 else -1)


# ### splitting into train and test data

# In[12]:

train_indices = pd.read_json('module-2-assignment-train-idx.json')
train_indices_list = list(train_indices[0])


# In[13]:

test_indices = pd.read_json('module-2-assignment-test-idx.json')
test_indices_list = list(test_indices[0])


# In[14]:

#splitting the dataset into test and train
train_data = products.iloc[train_indices_list]
test_data = products.iloc[test_indices_list]


# In[15]:

test_data


# ### creating a bag of words feature matrix 

# In[16]:

#bag of words feature

from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer(token_pattern=r'\b\w+\b')
# Use this token pattern to keep single-letter words
# First, learn vocabulary from the training data and assign columns to words
# Then convert the training data into a sparse matrix
train_matrix = vectorizer.fit_transform(train_data['review_clean'])
# Second, convert the test data into a sparse matrix, using the same word-column mapping
test_matrix = vectorizer.transform(test_data['review_clean'])


# ### training a logistic regression model

# In[17]:

from sklearn.linear_model import LogisticRegression

sentiment_model = LogisticRegression(penalty='l2')
sentiment_model = sentiment_model.fit(train_matrix, train_data['sentiment'])


# In[18]:

#count number of positive coeffs
pos_coeffs = sentiment_model.coef_ >= 0
Counter(list(pos_coeffs[0]))


# ### Making Predictions with Logistic Regression

# In[19]:

sample_test_data = test_data.iloc[10:13,].copy()
sample_test_data


# In[20]:

sample_test_matrix = vectorizer.transform(sample_test_data['review_clean'])
scores = sentiment_model.decision_function(sample_test_matrix)

print scores


# In[21]:

def predict_sentiment(scores, data):
    x = []
    for score in scores:
        if score > 0:
            x.append(+1)
        else:
            x.append(-1)
    data['predicted_sentiment'] = x
    
predict_sentiment(scores, sample_test_data)


# In[84]:

sample_test_data


# In[23]:

def predict_probabs(scores, data):
    x = []
    for score in scores:
        probab = 1/(1+(math.exp(-score)))
        x.append(probab)
    data['probab+1'] = x

predict_probabs(scores, sample_test_data)


# In[24]:

temp = sentiment_model.predict_proba(sample_test_matrix)
pd.DataFrame(temp)


# In[25]:

#predict sentiment for the whole test data
predictions_test = sentiment_model.predict(test_matrix)
test_data['predicted_sentiments'] = list(pd.DataFrame(predictions_test)[0])


# In[26]:

predictions_probab_test = sentiment_model.predict_proba(test_matrix)
test_data['positive_probab_pred'] = list(pd.DataFrame(predictions_probab_test)[1])


# In[27]:

test_data.sort_values(['positive_probab_pred'],ascending=True)


# ### calculating the accuracy of the model

# In[28]:

accur_temp = test_data['predicted_sentiments'] == test_data['sentiment']


# In[29]:

accuracy = float(accur_temp.value_counts()[1])/float(np.sum(accur_temp.value_counts()))
print ("%.2f" % accuracy)


# ### classifier with fewer words

# In[30]:

significant_words = ['love', 'great', 'easy', 'old', 'little', 'perfect', 'loves', 
      'well', 'able', 'car', 'broke', 'less', 'even', 'waste', 'disappointed', 
      'work', 'product', 'money', 'would', 'return']


# In[31]:

vectorizer_word_subset = CountVectorizer(vocabulary=significant_words) # limit to 20 words
train_matrix_word_subset = vectorizer_word_subset.fit_transform(train_data['review_clean'])
test_matrix_word_subset = vectorizer_word_subset.transform(test_data['review_clean'])


# In[45]:

simple_model = LogisticRegression(penalty='l2')
simple_model = simple_model.fit(train_matrix_word_subset, train_data['sentiment'])


simple_model_coef_table = pd.DataFrame(
    {
        'words': significant_words,
        'coeffs': list(simple_model.coef_.flatten())
    }
)
simple_model_coef_table


# ### comparing accuracy of sentiment model and simple model

# In[64]:

#train data
simple_model_train_preds = simple_model.predict(train_matrix_word_subset)
sentiment_model_train_preds = sentiment_model.predict(train_matrix)

train_sentiments = pd.DataFrame({
    'true_sentiments': list(train_data['sentiment']),
    'simple_model': list(simple_model_train_preds),
    'sentiment_model': list(sentiment_model_train_preds)
})


# In[65]:

temp1 = train_sentiments['true_sentiments'] == train_sentiments['simple_model']
simple_model_train_accuracy = float(temp1.value_counts()[1])/float(np.sum(temp1.value_counts()))
print ("%.2f" % simple_model_train_accuracy)


# In[60]:

temp2 = train_sentiments['true_sentiments'] == train_sentiments['sentiment_model']
senti_model_train_accuracy = float(temp2.value_counts()[1])/float(np.sum(temp2.value_counts()))
print ("%.2f" % senti_model_train_accuracy)


# In[66]:

#test data
simple_model_test_preds = simple_model.predict(test_matrix_word_subset)
sentiment_model_test_preds = sentiment_model.predict(test_matrix)

test_sentiments = pd.DataFrame({
    'true_sentiments': list(test_data['sentiment']),
    'simple_model': list(simple_model_test_preds),
    'sentiment_model': list(sentiment_model_test_preds)
})


# In[74]:

temp3 = test_sentiments['true_sentiments'] == test_sentiments['simple_model']
simple_model_test_accuracy = float(temp3.value_counts()[1])/float(np.sum(temp3.value_counts()))
print ("%.2f" % simple_model_test_accuracy)


# In[75]:

temp4 = test_sentiments['true_sentiments'] == test_sentiments['sentiment_model']
sentiment_model_test_accuracy = float(temp4.value_counts()[1])/float(np.sum(temp4.value_counts()))
print ("%.2f" % sentiment_model_test_accuracy)


# ### comparing with the majority class classifier

# In[83]:

test_sentiments['majority'] = +1

temp5 = test_sentiments['true_sentiments'] == test_sentiments['majority']
majority_model_test_accuracy = float(temp5.value_counts()[1])/float(np.sum(temp5.value_counts()))
print ("%.2f" % majority_model_test_accuracy)

