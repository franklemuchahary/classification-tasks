# README #

Practicing Machine Learning - Classification

Learning the various concepts related to classification.
This repository includes tasks related to the following topics
applied to a loan default prediction dataset:

1. Binary Classification and Multiclass Classification

2. Logistic Regression

3. Decision Tress

4. Ensemble Methods like Boosted Trees

5. Stochastic Gradient Descent applied to very large datasets